/*
 * Copyright 2017 Thomás Sousa Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.togetherdev.util.security;

import java.security.SecureRandom;
import java.util.Base64.Encoder;
import java.util.concurrent.ThreadLocalRandom;

/**
 * This class is useful to generate secure random codes.
 *
 * @author Thomás Sousa Silva
 */
public class DefaultSecureRandomizer implements SecureRandomizer {

	private final int SEED_LENGTH;
	private final long SEED_DURATION;
	private final int MAX_LENGTH;
	private final int MIN_LENGTH;
	private long generatedCodesAmount;
	private final Encoder ENCODER;
	private final SecureRandom SECURE_RANDOMIZER;

	/**
	 * Constructs a new instance of DefaultPasswordDigester with the builder config.
	 *
	 * @param builder The builder with the desired config.
	 * @throws NullPointerException If the builder is null.
	 */
	protected DefaultSecureRandomizer(SecureRandomizer.Builder builder) {
		SEED_LENGTH = builder.seedLength;
		SEED_DURATION = builder.seedDuration;
		MAX_LENGTH = builder.maxLength;
		MIN_LENGTH = builder.minLength;
		ENCODER = builder.encoder;
		SECURE_RANDOMIZER = builder.secureRandomizer;
	}

	@Override
	public byte[] fillArrayRandomly(byte[] array) {
		SECURE_RANDOMIZER.nextBytes(array);
		return array;
	}

	/**
	 * {@inheritDoc}
	 * <p>
	 * By default this method use the {@linkplain ThreadLocalRandom#nextInt(int, int) nextInt method} to generate the array length.
	 * </p>
	 */
	@Override
	public byte[] generateRandomArray() {
		byte[] bytes = new byte[generateCodeLength()];
		SECURE_RANDOMIZER.nextBytes(bytes);
		incrementCodeAmount();
		return bytes;
	}

	@Override
	public Encoder getEncoder() {
		return ENCODER;
	}

	@Override
	public int getMaxLength() {
		return MAX_LENGTH;
	}

	@Override
	public int getMinLength() {
		return MIN_LENGTH;
	}

	@Override
	public String getSecureAlgorithm() {
		return SECURE_RANDOMIZER.getAlgorithm();
	}

	@Override
	public long getSeedDuration() {
		return SEED_DURATION;
	}

	@Override
	public int getSeedLength() {
		return SEED_LENGTH;
	}

	@Override
	public String toString() {
		return new StringBuilder()
				.append("DefaultSecureRandomizer [SEED_LENGTH=").append(SEED_LENGTH)
				.append(", SEED_DURATION=").append(SEED_DURATION)
				.append(", MAX_LENGTH=").append(MAX_LENGTH)
				.append(", MIN_LENGTH=").append(MIN_LENGTH)
				.append(", generatedCodesAmount=").append(generatedCodesAmount)
				.append(", ENCODER=").append(ENCODER)
				.append(", SECURE_RANDOMIZER=").append(SECURE_RANDOMIZER)
				.append("]").toString();
	}

	/**
	 * @return A number between the defined {@link #MIN_LENGTH minimum} (Inclusive) and {@link #MAX_LENGTH maximum} (Exclusive) length.
	 */
	private int generateCodeLength() {
		if (MIN_LENGTH == MAX_LENGTH) {
			return MIN_LENGTH;
		}
		return ThreadLocalRandom.current().nextInt(MIN_LENGTH, MAX_LENGTH);
	}

	/**
	 * If the seed duration is larger than zero, this method increments the generated code amount and if the new amount is larger or equal the seed
	 * duration, then the seed is changed and the count is zeroed.
	 *
	 * @see #SEED_DURATION SEED_DURATION
	 * @see #generatedCodesAmount generatedCodesAmount
	 * @see #SEED_LENGTHSEED_LENGTH
	 * @see SecureRandom#setSeed(byte[]) setSeed
	 * @see SecureRandom#generateSeed(int) generateSeed
	 */
	private void incrementCodeAmount() {
		if (SEED_DURATION > 0) {
			generatedCodesAmount++;
			if (generatedCodesAmount >= SEED_DURATION) {
				SECURE_RANDOMIZER.setSeed(SECURE_RANDOMIZER.generateSeed(SEED_LENGTH));
				generatedCodesAmount = 0;
			}
		}
	}

	/**
	 * The default builder of instances of DefaultSecureRandomizer
	 */
	public static class Builder extends SecureRandomizer.Builder {

		@Override
		public SecureRandomizer build() {
			return new DefaultSecureRandomizer(this);
		}

	}

}
