/*
 * Copyright 2017 Thomás Sousa Silva
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License.  You may obtain a copy
 * of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.  See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */

package com.togetherdev.util.security;

/**
 * Represents the secure random algorithm name.
 *
 * @author Thomás Sousa Silva
 */
public enum SecureRandomAlgorithm {

	/**
	 * Obtains random numbers from the underlying installed and configured PKCS11 library.
	 */
	PKCS11("PKCS11"),
	/**
	 * The name of the pseudo-random number generation (PRNG) algorithm supplied by the SUN provider. This algorithm uses SHA-1 as the foundation of
	 * the PRNG. It computes the SHA-1 hash over a true-random seed value concatenated with a 64-bit counter which is incremented by 1 for each
	 * operation. From the 160-bit SHA-1 output, only 64 bits are used.
	 */
	SHA1PRNG("SHA1PRNG"),
	/**
	 * Obtains random numbers from the underlying native OS. No assertions are made as to the blocking nature of generating these numbers.
	 */
	NATIVE_PRNG("NativePRNG"),
	/**
	 * Obtains random numbers from the underlying Windows OS.
	 */
	WINDOWS_PRNG("Windows-PRNG"),
	/**
	 * Obtains random numbers from the underlying native OS, blocking if necessary. For example, /dev/random on UNIX-like systems.
	 */
	NATIVE_PRNG_BLOCKING("NativePRNGBlocking"),
	/**
	 * Obtains random numbers from the underlying native OS, without blocking to prevent applications from excessive stalling. For example,
	 * /dev/urandom on UNIX-like systems.
	 */
	NATIVE_PRNG_NON_BLOCKING("NativePRNGNonBlocking");

	private final String ALGORITHM_NAME;

	private SecureRandomAlgorithm(String algorithmName) {
		ALGORITHM_NAME = algorithmName;
	}

	/**
	 * @return The algorithm name.
	 */
	@Override
	public String toString() {
		return ALGORITHM_NAME;
	}

}
