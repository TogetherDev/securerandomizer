# Secure Randomizer #

#Versions

* **1.0**

## What is it?

This is a utility library to generate random secure codes of random length, that can be used to various purposes, for example to generate salts and password recovery codes.

## Using Secure Randomizer

You can use our maven repository, that is easier, or you can download it and build the Jar file and later add to your project.

## Maven Repository

If in your pom.xml file have not the [TogetherDev Maven Repository](https://bitbucket.org/TogetherDev/mavenrepository) then, add this code.

```
#!xml

<repository>
    <id>togetherdev</id>
    <url>https://bitbucket.org/TogetherDev/mavenrepository/raw/master</url>
</repository>

```

And then, add this dependency:

```
#!xml
<dependency>
    <groupId>com.togetherdev</groupId>
    <artifactId>secure-randomizer</artifactId>
    <version>1.0</version>
</dependency>
```

## Use example:

```
#!java

 public class UserService {

 	private static final SecureRandomizer SECURE_RANDOMIZER = SecureRandomizer.builder()
			.setCodeLength(20, 26)
			.setSecureRandomizer(SHA1PRNG)
			.setSeedDuration(1_000_000, 10) // Optional
			.build();

	public void someMethod() {
		String code = SECURE_RANDOMIZER.generateRandomCodeInHex();
 	}

 }

```

## Licensing

**Secure Randomizer** is provided and distributed under the [Apache Software License 2.0](http://www.apache.org/licenses/LICENSE-2.0).

Refer to *LICENSE.txt* for more information.
